1 How to get Doxygen working with server-side search

Most Ubuntu packages of Doxygen (until Ubuntu 14.10) do not include the doxyindexer/doxysearch.cgi tools necessary to build the search index from the searchdata.xml file written by Doxygen, even if they already existed in the given Doxygen version.

To get doxyindexer and doxysearch.cgi to work, follow these steps:
1. The Doxygen package from Ubuntu should be uninstalled
2. sudo apt-get install libxapian-dev  # http://en.wikipedia.org/wiki/Xapian 
3. git clone https://github.com/doxygen/doxygen.git
4. git checkout Release_1_8_6  # Change to the desired version (e.g. 1.8.6 to stay compatible with the Ubuntu 14.04 package)
5. ./configure # Check if all packages are available.
6. make -> sudo make install 
7. cd addon/doxysearch/ -> make -> sudo make install
   (everything is installed to /usr/local/)

To allow doxysearch.cgi to be run (instead of serving the binary as a download to the client) modify the Apache2 config:
<Directory /var/www/doc/armarxdoc/>  # adapt path, if necessary
    <Files doxysearch.cgi>
        Options +ExecCGI
        AddHandler cgi-script .cgi
    </Files>
</Directory>


2 How to use doxyindexer/doxysearch.cgi on command line

Doxygen generates a file called "searchdata.xml" in the output directory.

1. To put the raw search data into an index run this from the command line:
   doxyindexer searchdata.xml
   This will create a directory called doxysearch.db with some files in it.
2. Copy the doxysearch.db directory to the same directory as where the doxysearch.cgi is located.
3. To search a phrase using doxysearch.cgi run for example this from the command line:
   doxysearch.cgi q=queryString&n=20&p=0
   The following fields are passed:
   * q : query text as entered by the user
   * n : number of search results requested
   * p : number of search page for which to return the results;each page has n values


3 Further information about searching

External Indexing and Searching: http://www.stack.nl/~dimitri/doxygen/manual/extsearch.html
